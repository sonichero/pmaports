# Reference: <https://postmarketos.org/vendorkernel>
# Kernel config based on: arch/arm64/configs/msm8916_defconfig

_flavor="postmarketos-qcom-msm8916"
pkgname="linux-${_flavor}"
pkgver=5.4.0_rc2
pkgrel=0
pkgdesc="Mainline kernel fork for Qualcomm MSM8916 devices"
arch="aarch64"
_carch="arm64"
url="https://github.com/msm8916-mainline/linux"
license="GPL-2.0-only"
options="!strip !check !tracedeps"
depends_dev="perl gmp-dev elfutils-dev bash flex bison"
makedepends="$depends_dev sed installkernel bc linux-headers openssl-dev dtbtool"

# Compiler: latest GCC from Alpine
HOSTCC="${CC:-gcc}"
HOSTCC="${HOSTCC#${CROSS_COMPILE}}"

# Source
_tag=v5.4-rc2-msm8916
source="
	$pkgname-$_tag.tar.gz::$url/archive/$_tag.tar.gz
	config-$_flavor.$arch
"
builddir="$srcdir/linux-${_tag#v}"

prepare() {
	default_prepare
	cp "$srcdir/config-$_flavor.$arch" .config
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION=$((pkgrel + 1 ))
}

package() {
	mkdir -p "$pkgdir"/boot
	make zinstall modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_PATH="$pkgdir"/boot \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_DTBS_PATH="$pkgdir"/usr/share/dtb
	rm -f "$pkgdir"/lib/modules/*/build "$pkgdir"/lib/modules/*/source

	install -D "$builddir"/include/config/kernel.release \
		"$pkgdir"/usr/share/kernel/$_flavor/kernel.release

	# Master DTB (deviceinfo_bootimg_qcdt)
	dtbTool -p scripts/dtc/ -o arch/arm64/boot/dt.img arch/arm64/boot/
	install -Dm644 arch/arm64/boot/dt.img "$pkgdir"/boot/dt.img
}

sha512sums="d11115cbdb2c091bb65375e9b3a2d6f0315b5a24565bce9be7765b8214189f130d49560498c0ead8b4b341ebef736cc8deac784a7aab594b3b10dd57cc35caa0  linux-postmarketos-qcom-msm8916-v5.4-rc2-msm8916.tar.gz
1341726bdfeba54e913afa43d56c1ea58fc4f5b8e1d0b064297b9fbf7645e4c21a0a6e695d0532cbd0d4d953257df54fd564c9e931462678e2ec88b56ba39eb6  config-postmarketos-qcom-msm8916.aarch64"
